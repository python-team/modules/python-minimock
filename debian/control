Source: python-minimock
Priority: optional
Maintainer: Debian QA Group <packages@qa.debian.org>
Section: python
Homepage: https://pypi.python.org/pypi/MiniMock
Vcs-Git: https://salsa.debian.org/python-team/packages/python-minimock.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-minimock
Build-Depends:
    debhelper-compat (= 13),
    dh-python,
    python3-setuptools,
    python3-all
Standards-Version: 4.6.1

Package: python3-minimock
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}
Provides: ${python3:Provides}
Breaks: ${python3:Breaks}
Description: simple library for Python mock objects
 minimock is a simple Python library for using mock objects.
 .
 Its mock objects will report any access made to the mock object's
 interfaces. The programmer can easily make assertions about how mock
 objects are used in the test cases, by using the standard-library
 ‘doctest’ module to match the reported access against expected
 behaviour.
 .
 Mock objects can return specified values, raise exceptions, etc.
 to simulate the mocked behaviour. Existing objects can optionally
 be replaced in-place in their namespace by a mock object, and
 restored safely after testing.
